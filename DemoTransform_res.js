function transformRes(line){
    var values = line.split(',');
    var obj = new Object();
    obj.nombreequipo = values[0];
    obj.companeros   = values[1];
    obj.salida       = values[2];
    obj.becerros     = values[3];
    obj.tiempo       = values[4];
    var jsonString = JSON.stringify(obj);
    return jsonString;
}